module aloqa-bank/ab_go_api_gateway

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/minio/minio-go/v7 v7.0.23
	github.com/spf13/cast v1.4.1
	github.com/swaggo/gin-swagger v1.3.3
	github.com/swaggo/swag v1.7.6
	github.com/xtgo/uuid v0.0.0-20140804021211-a0b114877d4c
	go.uber.org/zap v1.19.1
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
