package handlers

import (
	"aloqa-bank/ab_go_api_gateway/api/http"
	"context"

	"aloqa-bank/ab_go_api_gateway/genproto/content_service"

	"aloqa-bank/ab_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateFreqAksed godoc
// @ID create_freq_asked
// @Router /freq-asked [POST]
// @Summary Create FreqAsked
// @Description Create FreqAsked
// @Tags FreqAsked
// @Accept json
// @Produce json
// @Param faq body content_service.FreqAskRequest true "freq-asked"
// @Success 201 {object} http.Response{data=content_service.FreqAsk} "FREQASKEDBody"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateFreqAksed(c *gin.Context) {
	var faq content_service.FreqAskRequest

	err := c.ShouldBindJSON(&faq)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}
	
	resp, err := h.services.FreqAskedService().Create(
		context.Background(),
		&faq,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetFreqAskedList godoc
// @ID get_freq_asked_list
// @Router /freq-asked [GET]
// @Summary Get FREQASKED List
// @Description  Get FREQASKED List
// @Tags FreqAsked
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=content_service.GetFreqAskListResponse} "GetFAQListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetFreqAskedList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.FreqAskedService().GetList(
		context.Background(),
		&content_service.GetFreqAskListRequest{
			Limit:  int32(limit),
			Offset: int32(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetFreqAskedByID godoc
// @ID get_freq_asked_by_id
// @Router /freq-asked/{freq-asked-id} [GET]
// @Summary Get FREQAsked By ID
// @Description Get FREQAsked By ID
// @Tags FreqAsked
// @Accept json
// @Produce json
// @Param freq-asked-id path string true "freq-asked-id"
// @Success 200 {object} http.Response{data=content_service.FreqAsk} "FREQASKEDBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetFreqAskedByID(c *gin.Context) {
	faqID := c.Param("freq-asked-id")

	if !util.IsValidUUID(faqID) {
		h.handleResponse(c, http.InvalidArgument, "freq-asked is an invalid uuid")
		return
	}

	resp, err := h.services.FreqAskedService().GetByID(
		context.Background(),
		&content_service.GetById{
			Id: faqID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateFreqAsked godoc
// @ID update_freq_asked
// @Router /freq-asked [PUT]
// @Summary Update FREQAsked
// @Description Update FREQAsked
// @Tags FreqAsked
// @Accept json
// @Produce json
// @Param faq body content_service.FreqAsk true "freq-asked"
// @Success 200 {object} http.Response{data=content_service.FreqAsk} "FREQASKEDBody"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateFreqAsked(c *gin.Context) {
	var faq content_service.FreqAsk

	err := c.ShouldBindJSON(&faq)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.FreqAskedService().Update(
		context.Background(),
		&faq,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteFreqAsked godoc
// @ID delete_freq_asked
// @Router /freq-asked/{freq-asked-id} [DELETE]
// @Summary Delete FREQASKED
// @Description Delete FREQASKED
// @Tags FreqAsked
// @Accept json
// @Produce json
// @Param freq-asked-id path string true "freq-asked-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteFreqAsked(c *gin.Context) {
	faqID := c.Param("freq-asked-id")

	if !util.IsValidUUID(faqID) {
		h.handleResponse(c, http.InvalidArgument, "freq id is an invalid uuid")
		return
	}

	resp, err := h.services.FreqAskedService().Delete(
		context.Background(),
		&content_service.GetById{
			Id: faqID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
