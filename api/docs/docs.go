// Package docs GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag
package docs

import (
	"bytes"
	"encoding/json"
	"strings"
	"text/template"

	"github.com/swaggo/swag"
)

var doc = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "termsOfService": "https://udevs.io",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/config": {
            "get": {
                "description": "show service config data when the service environment set to debug mode",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "get config data on the debug mode",
                "operationId": "get_config",
                "responses": {
                    "200": {
                        "description": "Response data",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/config.Config"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "$ref": "#/definitions/http.Response"
                        }
                    }
                }
            }
        },
        "/freq-asked": {
            "get": {
                "description": "Get FREQASKED List",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FreqAsked"
                ],
                "summary": "Get FREQASKED List",
                "operationId": "get_freq_asked_list",
                "parameters": [
                    {
                        "type": "integer",
                        "description": "offset",
                        "name": "offset",
                        "in": "query"
                    },
                    {
                        "type": "integer",
                        "description": "limit",
                        "name": "limit",
                        "in": "query"
                    },
                    {
                        "type": "string",
                        "description": "search",
                        "name": "search",
                        "in": "query"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "GetFAQListResponseBody",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/content_service.GetFreqAskListResponse"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "400": {
                        "description": "Invalid Argument",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "500": {
                        "description": "Server Error",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            },
            "put": {
                "description": "Update FREQAsked",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FreqAsked"
                ],
                "summary": "Update FREQAsked",
                "operationId": "update_freq_asked",
                "parameters": [
                    {
                        "description": "freq-asked",
                        "name": "faq",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/content_service.FreqAsk"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "FREQASKEDBody",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/content_service.FreqAsk"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "500": {
                        "description": "Server Error",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            },
            "post": {
                "description": "Create FreqAsked",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FreqAsked"
                ],
                "summary": "Create FreqAsked",
                "operationId": "create_freq_asked",
                "parameters": [
                    {
                        "description": "freq-asked",
                        "name": "faq",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/content_service.FreqAskRequest"
                        }
                    }
                ],
                "responses": {
                    "201": {
                        "description": "FREQASKEDBody",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/content_service.FreqAsk"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "500": {
                        "description": "Server Error",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        },
        "/freq-asked/{freq-asked-id}": {
            "get": {
                "description": "Get FREQAsked By ID",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FreqAsked"
                ],
                "summary": "Get FREQAsked By ID",
                "operationId": "get_freq_asked_by_id",
                "parameters": [
                    {
                        "type": "string",
                        "description": "freq-asked-id",
                        "name": "freq-asked-id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "FREQASKEDBody",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "$ref": "#/definitions/content_service.FreqAsk"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "400": {
                        "description": "Invalid Argument",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "500": {
                        "description": "Server Error",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            },
            "delete": {
                "description": "Delete FREQASKED",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "FreqAsked"
                ],
                "summary": "Delete FREQASKED",
                "operationId": "delete_freq_asked",
                "parameters": [
                    {
                        "type": "string",
                        "description": "freq-asked-id",
                        "name": "freq-asked-id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "204": {
                        "description": ""
                    },
                    "400": {
                        "description": "Invalid Argument",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "500": {
                        "description": "Server Error",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        },
        "/ping": {
            "get": {
                "description": "this returns \"pong\" messsage to show service is working",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "returns \"pong\" message",
                "operationId": "ping",
                "responses": {
                    "200": {
                        "description": "Response data",
                        "schema": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/http.Response"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "data": {
                                            "type": "string"
                                        }
                                    }
                                }
                            ]
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "$ref": "#/definitions/http.Response"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "config.Config": {
            "type": "object",
            "properties": {
                "authGRPCPort": {
                    "type": "string"
                },
                "authServiceHost": {
                    "type": "string"
                },
                "contentGRPCPort": {
                    "type": "string"
                },
                "contentServiceHost": {
                    "type": "string"
                },
                "defaultLimit": {
                    "type": "string"
                },
                "defaultOffset": {
                    "type": "string"
                },
                "environment": {
                    "description": "debug, test, release",
                    "type": "string"
                },
                "httpport": {
                    "type": "string"
                },
                "httpscheme": {
                    "type": "string"
                },
                "minioAccessKey": {
                    "type": "string"
                },
                "minioEndpoint": {
                    "type": "string"
                },
                "minioSecretKey": {
                    "type": "string"
                },
                "orderGRPCPort": {
                    "type": "string"
                },
                "orderServiceHost": {
                    "type": "string"
                },
                "partnerGRPCPort": {
                    "type": "string"
                },
                "partnerServiceHost": {
                    "type": "string"
                },
                "serviceHost": {
                    "type": "string"
                },
                "serviceName": {
                    "type": "string"
                },
                "version": {
                    "type": "string"
                }
            }
        },
        "content_service.FreqAsk": {
            "type": "object",
            "properties": {
                "description": {
                    "type": "string"
                },
                "id": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "content_service.FreqAskRequest": {
            "type": "object",
            "properties": {
                "description": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "content_service.GetFreqAskListResponse": {
            "type": "object",
            "properties": {
                "count": {
                    "type": "integer"
                },
                "freq_ask": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/content_service.FreqAsk"
                    }
                }
            }
        },
        "http.Response": {
            "type": "object",
            "properties": {
                "data": {},
                "description": {
                    "type": "string"
                },
                "status": {
                    "type": "string"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Schemes     []string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = swaggerInfo{
	Version:     "",
	Host:        "",
	BasePath:    "",
	Schemes:     []string{},
	Title:       "",
	Description: "This is a api gateway",
}

type s struct{}

func (s *s) ReadDoc() string {
	sInfo := SwaggerInfo
	sInfo.Description = strings.Replace(sInfo.Description, "\n", "\\n", -1)

	t, err := template.New("swagger_info").Funcs(template.FuncMap{
		"marshal": func(v interface{}) string {
			a, _ := json.Marshal(v)
			return string(a)
		},
		"escape": func(v interface{}) string {
			// escape tabs
			str := strings.Replace(v.(string), "\t", "\\t", -1)
			// replace " with \", and if that results in \\", replace that with \\\"
			str = strings.Replace(str, "\"", "\\\"", -1)
			return strings.Replace(str, "\\\\\"", "\\\\\\\"", -1)
		},
	}).Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, sInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register("swagger", &s{})
}
