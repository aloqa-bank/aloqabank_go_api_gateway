package models

type UploadResponse struct {
	Filename string `json:"filename"`
}

type DownloadRequest struct {
	Filename string `json:"filename"`
}

type DeleteRequest struct {
	Filename string `json:"filename"`
}
