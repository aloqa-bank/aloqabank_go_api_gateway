package models

type GetProjectMemberListResponse struct {
	Count          int32            `json:"count"`
	ProjectMembers []*ProjectMember `json:"project_members"`
}

type ProjectMember struct {
	Id            string `json:"id"`
	ProjectId     string `json:"project_id"`
	MemberGroupId string `json:"member_group_id"`
	UserId        string `json:"user_id"`
	SphereId      string `json:"sphere_id"`
	PositionId    string `json:"position_id"`
	User          User   `json:"user"`
}

type User struct {
	Id               string `json:"id"`
	ProjectId        string `json:"project_id"`
	ClientPlatformId string `json:"client_platform_id"`
	ClientTypeId     string `json:"client_type_id"`
	RoleId           string `json:"role_id"`
	Phone            string `json:"phone"`
	Email            string `json:"email"`
	Login            string `json:"login"`
	Password         string `json:"password"`
	Active           int32  `json:"active"`
	ExpiresAt        string `json:"expires_at"`
	CreatedAt        string `json:"created_at"`
	UpdatedAt        string `json:"updated_at"`
	Name             string `json:"name"`
	PhotoUrl         string `json:"photo_url"`
}
