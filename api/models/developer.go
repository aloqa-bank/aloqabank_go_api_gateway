package models

type Language struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Address     string `json:"address"`
}
type LanguageData struct {
	Ru      Language `json:"ru"`
	UzLatin Language `json:"uz_latin"`
	UzKiril Language `json:"uz_kiril"`
}
type DeveloperWithStatus struct {
	Id                      string       `json:"id"`
	UserId                  string       `json:"user_id"`
	StatusId                string       `json:"status_id"`
	Logo                    string       `json:"logo"`
	Date                    string       `json:"date"`
	ResidentialComplexCount int32        `json:"residential_complex_count"`
	ContactNumber           string       `json:"contact_number"`
	OrderNumber             int32        `json:"order_number"`
	CreatedAt               string       `json:"created_at"`
	LanguageData            LanguageData `json:"language_data"`
	Status                  *Status      `json:"status"`
}

type GetResponseDeveloperList struct {
	Count      int32                 `json:"count"`
	Developers []DeveloperWithStatus `json:"developers"`
}
