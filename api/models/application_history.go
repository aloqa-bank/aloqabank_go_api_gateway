package models

type ChangedOwner struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phone_number"`
}

type ApplicationHistory struct {
	Id                     string             `json:"id"`
	BankId                 string             `json:"bank_id"`
	ApplicationId          string             `json:"application_id"`
	StatusId               string             `json:"status_id"`
	CustomerId             string             `json:"customer_id"`
	ResidentialApartmentId string             `json:"residential_apartment_id"`
	Comment                string             `json:"comment"`
	ActionTime             string             `json:"action_time"`
	OrderNumber            int32              `json:"order_number"`
	ChangedOwner           *ChangedOwner      `json:"changed_owner"`
	Status                 *ApplicationStatus `json:"status"`
}

type GetApplicationHistoryResponse struct {
	Count                int32                `json:"count"`
	ApplicationHistories []ApplicationHistory `json:"application_histories"`
}
