package models

// represents the count of applications
// in each status
type StatusCount struct {
	ID      string `json:"id"`
	OwnerID string `json:"owner_id"`
	Label   string `json:"label"`
	Count   int32  `json:"count"`
	Color   string `json:"color"`
}

type ClientInfo struct {
	Pinfl          string `json:"pinfl"`
	PassportNumber uint32 `json:"passport_number"`
	PassportSeries string `json:"passport_series"`
	Phone          string `json:"phone"`
	Dob            string `json:"dob"` // date of birthday
	Email          string `json:"email"`
	Gender         string `json:"gender"`
	Firstname      string `json:"firstname"` // ism
	Surname        string `json:"surname"`   // familya
	Lastname       string `json:"lastname"`  // sharif
	Inn            string `json:"inn"`
}

type IpotekaOption struct {
	ProductName       string `json:"product_name"`
	IpotekaDeadline   string `json:"ipoteka_deadline"` // srok kredita
	IpotekaCost       string `json:"ipoteka_cost"`
	IpotekaCurrency   string `json:"ipoteka_currency"`
	CustomerRegion    string `json:"customer_region"`
	CustomerDistrict  string `json:"customer_district"`
	MonthlyPaymentDay uint32 `json:"monthly_payment_day"`
	GracePeriod       uint32 `json:"grace_period"` // Количество пунктов льготново периода для ОД
	LoanObjective     string `json:"loan_objective"`
}

type Questionnaire struct {
	ExperienceYears           uint32 `json:"experience_years"`
	ContiniousExperienceYears uint32 `json:"continious_experience_years"`
	ResidenceAddress          string `json:"residence_address"`
	MaritalStatus             string `json:"marital_status"`     // semeynoye polojeniye
	OwnershipProperty         string `json:"ownership_property"` // Наличие в собственности имущества
	EducationLevel            string `json:"education_level"`
}

type Documents struct {
	Id       string `json:"id"`
	Title    string `json:"title"`
	Filename string `json:"filename"`
}

type Application struct {
	CustomerId             string             `json:"customer_id"`
	ResidentialApartmentId string             `json:"residential_apartment_id"`
	BankId                 string             `json:"bank_id"`
	StatusId               string             `json:"status_id"`
	Id                     string             `json:"id"`
	LoanObjective          string             `json:"loan_objective"`
	LoanType               string             `json:"loan_type"`
	Bid                    float32            `json:"bid"`
	LegalApplicationId     string             `json:"legal_application_id"`
	CustomerInpNumber      string             `json:"customer_inp_number"`
	BankCode               string             `json:"bank_code"`
	OrganisationInn        string             `json:"organisation_inn"`
	CompanyName            string             `json:"company_name"`
	OrderedDate            string             `json:"ordered_date"`
	Status                 *ApplicationStatus `json:"status"`
	ClientInfo             *ClientInfo        `json:"client_info"`
	IpotekaOption          *IpotekaOption     `json:"ipoteka_option"`
	Questionnaire          *Questionnaire     `json:"questionnaire"`
	Documents              []*Documents       `json:"documents"`
	OrderNumber            string             `json:"order_number"`
	IsCompleted            bool               `json:"is_completed"`
	ApplicationId          int32              `json:"application_id"`
}

type GetApplicationListResponse struct {
	CountAll     int32          `json:"count_all"`
	Count        int32          `json:"count"`
	StatusCount  []*StatusCount `json:"status_count"`
	Applications []*Application `json:"applications"`
}

type CreateInitialApplicationRequest struct {
	CustomerId             string `json:"customer_id"`
	ResidentialApartmentId string `json:"residential_apartment_id"`
	BankId                 string `json:"bank_id"`
}
