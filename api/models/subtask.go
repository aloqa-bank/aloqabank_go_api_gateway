package models

type SubtaskPrimaryKey struct {
	SubtaskId string
}

type ChecklistItem struct {
	Id        string `json:"id"`
	Title     string `json:"title"`
	Status    string `json:"status"`
	CreatorId string `json:"creator_id"`
}

type CommentItem struct {
	Id          string `json:"id"`
	CreatorId   string `json:"creator_id"`
	CreatorName string `json:"creator_name"`
	Message     string `json:"message"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type Subtask struct {
	TaskId           string          `json:"task_id"`
	StageId          string          `json:"stage_id"`
	Id               string          `json:"id"`
	Title            string          `json:"title"`
	OrderNumber      int32           `json:"order_number"`
	BoardOrderNumber int32           `json:"board_order_number"`
	CreatorId        string          `json:"creator_id"`
	Description      string          `json:"description"`
	StatusId         string          `json:"status_id"`
	SprintId         string          `json:"sprint_id"`
	TaskTitle        string          `json:"task_title"`
	StageTitle       string          `json:"stage_title"`
	ChecklistItems   []ChecklistItem `json:"checklist_items"`
	CommentItems     []CommentItem   `json:"comment_items"`
	CountDone        string          `json:"count_done"`
	CountAll         string          `json:"count_all"`
	CreatedAt        string          `json:"created_at"`
	UpdatedAt        string          `json:"updated_at"`
	ProjectId        string          `json:"project_id"`
	VersionId        string          `json:"version_id"`
	StartTime        string          `json:"start_time"`
	EndTime          string          `json:"end_time"`
	AcceptedTime     string          `json:"accepted_time"`
	AssigneeUserIds  []string        `json:"assignee_user_ids"`
	AssigneeUsers    []User          `json:"assignee_users"`
}
