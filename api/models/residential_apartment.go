package models

type ResidentialApartmentWithStatus struct {
	Id                     string                       `json:"id"`
	DeveloperId            string                       `json:"developer_id"`
	ResidentialComplexId   string                       `json:"residential_complex_id"`
	CorpusId               string                       `json:"corpus_id"`
	BlockId                string                       `json:"block_id"`
	EntranceId             string                       `json:"entrance_id"`
	FloorId                string                       `json:"floor_id"`
	ApartmentId            string                       `json:"apartment_id"`
	Quantity               int32                        `json:"quantity"`
	StatusId               string                       `json:"status_id"`
	Status                 *Status                      `json:"status"`
	ApartmentData          ApartmentWithStatus          `json:"apartment_data"`
	ResidentialComplexData ResidentialComplexWithStatus `json:"residential_complex_data"`
}

type GetResponseResidentialApartmentList struct {
	Count                  int32                            `json:"count"`
	ResidentialApartmentes []ResidentialApartmentWithStatus `json:"residential_apartments"`
}
