package models

type FavouriteResidentialComplex struct {
	Id                     string                       `json:"id"`
	UserId                 string                       `json:"user_id"`
	ResidentialComplexId   string                       `json:"residential_complex_id"`
	OrderNumber            int32                        `json:"order_number"`
	ResidentialComplexData ResidentialComplexWithStatus `json:"residential_complex_data"`
}

type GetFavouriteResidentialComplexResponse struct {
	Count                       int32                         `json:"count"`
	FavouriteResidentialComplex []FavouriteResidentialComplex `json:"favourite_residential_complex"`
}
