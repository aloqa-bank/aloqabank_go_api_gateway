package models

type ApplicationStatus struct {
	Id          string `json:"id"`
	OwnerId     string `json:"owner_id"`
	Label       string `json:"label"`
	Color       string `json:"color"`
	OrderNumber int32  `json:"order_number"`
	// Type        string `json:"type"`
}

type ApplicationStatusItem struct {
	ApplicationStatus ApplicationStatus `json:"application_status"`
	Count             int32             `json:"count"`
}

type ApplicationStatuses struct {
	ApplicationStatuses []ApplicationStatus `json:"application_statuses"`
	Count               int32               `json:"count"`
}
