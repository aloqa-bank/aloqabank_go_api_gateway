package models

type Status struct {
	Id          string `json:"id"`
	Label       string `json:"label"`
	Color       string `json:"color"`
	OrderNumber int32  `json:"order_number"`
	Type        string `json:"type"`
}

type StatusItem struct {
	Status Status `json:"status"`
	Count  int32  `json:"count"`
}

type Statuses struct {
	Statuses []Status `json:"statuses"`
	Count    int32    `json:"count"`
}
