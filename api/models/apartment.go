package models

type RoomLanguage struct {
	Name string `json:"name"`
}

type RoomLanguageData struct {
	Ru      RoomLanguage `json:"ru"`
	UzLatin RoomLanguage `json:"uz_latin"`
	UzKiril RoomLanguage `json:"uz_kiril"`
}

type Rooms struct {
	Id           string           `json:"id"`
	OrderNumber  int32            `json:"order_number"`
	Area         float32          `json:"area"`
	LanguageData RoomLanguageData `json:"language_data"`
}

type Options struct {
	Id           string           `json:"id"`
	OrderNumber  int32            `json:"order_number"`
	Area         float32          `json:"area"`
	LanguageData RoomLanguageData `json:"language_data"`
}

type ApartmentLanguage struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type ApartmentLanguageData struct {
	Ru      ApartmentLanguage `json:"ru"`
	UzLatin ApartmentLanguage `json:"uz_latin"`
	UzKiril ApartmentLanguage `json:"uz_kiril"`
}

type ApartmentWithStatus struct {
	Id                   string                `json:"id"`
	ResidentialComplexId string                `json:"residential_complex_id"`
	StatusId             string                `json:"status_id"`
	Area                 float32               `json:"area"`
	Cost                 float32               `json:"cost"`
	ApartmentPictures    []string              `json:"apartment_pictures"`
	OrderNumber          int32                 `json:"order_number"`
	RoomQuantity         int32                 `json:"room_quantity"`
	LanguageData         ApartmentLanguageData `json:"language_data"`
	Status               *Status               `json:"status"`
	Rooms                []Rooms               `json:"rooms"`
	Options              []Options             `json:"options"`
}

type GetResponseApartmentList struct {
	Count      int32                 `json:"count"`
	Apartments []ApartmentWithStatus `json:"apartments"`
}
