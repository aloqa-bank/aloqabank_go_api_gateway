package models

type Location struct {
	Longtitude float32 `json:"longtitude"`
	Latitude   float32 `json:"latitude"`
}

type SimilarResComplex struct {
	Value        string       `json:"value"`
	LanguageData LanguageData `json:"language_data"`
}

type ResidentialComplexWithStatus struct {
	Id                        string              `json:"id"`
	DeveloperId               string              `json:"developer_id"`
	Gallery                   []string            `json:"gallery"`
	StatusId                  string              `json:"status_id"`
	ResidentialType           string              `json:"residential_type"`
	TotalArea                 float32             `json:"total_area"`
	TotalCost                 float32             `json:"total_cost"`
	PhoneNumber               string              `json:"phone_number"`
	BuildDate                 string              `json:"build_date"`
	MakeupCondition           string              `json:"makeup_condition"`
	OrderNumber               int32               `json:"order_number"`
	LocationMap               Location            `json:"location_map"`
	LocationText              string              `json:"location_text"`
	DeveloperLogo             string              `json:"developer_logo"`
	ApartmentMinPrice         float32             `json:"apartment_min_price"`
	ApartmentMaxPrice         float32             `json:"apartment_max_price"`
	CreatedAt                 string              `json:"created_at"`
	Recommendation            bool                `json:"recommendation"`
	LanguageData              LanguageData        `json:"language_data"`
	Status                    *Status             `json:"status"`
	SimilarResidentialComplex []SimilarResComplex `json:"similar_residential_complexes"`
}

type GetResponseResidentialComplexList struct {
	Count                int32                          `json:"count"`
	ResidentialComplexes []ResidentialComplexWithStatus `json:"residential_complexes"`
}
