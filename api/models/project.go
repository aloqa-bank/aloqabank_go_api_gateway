package models

type Item struct {
	Statuses   []StatusItem `json:"statuses"`
	TotalCount int32        `json:"total_count"`
	DoneCount  int32        `json:"done_count"`
}

// GetProjectReportResponse ...
type GetProjectReportResponse struct {
	Epic      Item `json:"epic"`
	Task      Item `json:"task"`
	Stage     Item `json:"stage"`
	Subtask   Item `json:"subtask"`
	Checklist Item `json:"checklist"`
}

// GetProjectVersionRequest ...
type GetProjectVersionRequest struct {
	ProjectId string `json:"project_id"`
	VersionId string `json:"version_id"`
}
