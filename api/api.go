package api

import (
	"aloqa-bank/ab_go_api_gateway/api/docs"
	"aloqa-bank/ab_go_api_gateway/api/handlers"
	"aloqa-bank/ab_go_api_gateway/config"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// requires swag version 1.7.6
// go install github.com/swaggo/swag/cmd/swag@v1.7.6
// @description This is a api gateway
// @termsOfService https://udevs.io
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	// docs.SwaggerInfo.Host = cfg.ServiceHost + cfg.HTTPPort
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())

	// FREQ-ASKED
	r.POST("/freq-asked", h.CreateFreqAksed)
	r.GET("/freq-asked", h.GetFreqAskedList)
	r.GET("/freq-asked/:freq-asked-id", h.GetFreqAskedByID)
	r.PUT("/freq-asked", h.UpdateFreqAsked)
	r.DELETE("/freq-asked/:freq-asked-id", h.DeleteFreqAsked)

	r.GET("/ping", h.Ping)
	r.GET("/config", h.GetConfig)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
