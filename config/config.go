package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServiceHost string
	HTTPPort    string
	HTTPScheme  string

	DefaultOffset string
	DefaultLimit  string

	MinioEndpoint  string
	MinioAccessKey string
	MinioSecretKey string

	PartnerServiceHost string
	PartnerGRPCPort    string

	ContentServiceHost string
	ContentGRPCPort    string

	OrderServiceHost string
	OrderGRPCPort    string

	AuthServiceHost string
	AuthGRPCPort    string
}

// Load ...
func Load() Config {
	if err := godotenv.Load(); err != nil {
		log.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "ab_go_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServiceHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":9100"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	config.MinioEndpoint = cast.ToString(getOrReturnDefaultValue("MINIO_ENDPOINT", "cdn.aloqabank.udevs.io"))
	config.MinioAccessKey = cast.ToString(getOrReturnDefaultValue("MINIO_ACCESS_KEY", "2wjyybzuy3g4ednkmpgmhczzdvfbk87d"))
	config.MinioSecretKey = cast.ToString(getOrReturnDefaultValue("MINIO_SECRET_KEY", "tgetjnczdxx9engvkthsy8cy8p25yqmz"))

	config.PartnerServiceHost = cast.ToString(getOrReturnDefaultValue("PARTNER_SERVICE_HOST", "localhost"))
	config.PartnerGRPCPort = cast.ToString(getOrReturnDefaultValue("PARTNER_GRPC_PORT", ":9101"))

	config.ContentServiceHost = cast.ToString(getOrReturnDefaultValue("CONTENT_SERVICE_HOST", "localhost"))
	config.ContentGRPCPort = cast.ToString(getOrReturnDefaultValue("CONTENT_GRPC_PORT", ":9102"))

	config.OrderServiceHost = cast.ToString(getOrReturnDefaultValue("ORDER_SERVICE_HOST", "localhost"))
	config.OrderGRPCPort = cast.ToString(getOrReturnDefaultValue("ORDER_GRPC_PORT", ":9103"))

	config.AuthServiceHost = cast.ToString(getOrReturnDefaultValue("AUTH_SERVICE_HOST", "0.0.0.0"))
	config.AuthGRPCPort = cast.ToString(getOrReturnDefaultValue("AUTH_GRPC_PORT", ":9104"))
	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
