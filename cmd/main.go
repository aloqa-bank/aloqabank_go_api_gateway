package main

import (
	"aloqa-bank/ab_go_api_gateway/api"
	"aloqa-bank/ab_go_api_gateway/config"
	"aloqa-bank/ab_go_api_gateway/pkg/logger"
	"aloqa-bank/ab_go_api_gateway/services"

	"aloqa-bank/ab_go_api_gateway/api/handlers"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	grpcSvcs, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger("ab_go_api_gateway", loggerLevel)
	defer logger.Cleanup(log)

	r := gin.New()

	r.Use(gin.Logger(), gin.Recovery())

	h := handlers.NewHandler(cfg, log, grpcSvcs)

	api.SetUpAPI(r, h, cfg)

	r.Run(cfg.HTTPPort)
}
