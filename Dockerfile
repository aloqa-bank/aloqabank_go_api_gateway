# workspace (GOPATH) configured at /go
FROM golang:1.16 as builder

#
RUN mkdir -p $GOPATH/src/aloqabank/aloqank_go_api_gateway
WORKDIR $GOPATH/src/aloqabank/aloqank_go_api_gateway

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    go mod vendor && \
    make build && \
    mv ./bin/aloqank_go_api_gateway /

ENTRYPOINT ["/aloqank_go_api_gateway"]
