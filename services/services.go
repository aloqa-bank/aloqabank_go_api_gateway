package services

import (
	"aloqa-bank/ab_go_api_gateway/config"
	"aloqa-bank/ab_go_api_gateway/genproto/content_service"

	"google.golang.org/grpc"
)

type ServiceManagerI interface {
	FreqAskedService() content_service.FreqAskedServiceClient
}

type grpcClients struct {
	freqAskedService content_service.FreqAskedServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connContentService, err := grpc.Dial(
		cfg.ContentServiceHost+cfg.ContentGRPCPort,
		grpc.WithInsecure(),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		freqAskedService: content_service.NewFreqAskedServiceClient(connContentService),
	}, nil
}

func (g *grpcClients) FreqAskedService() content_service.FreqAskedServiceClient {
	return g.freqAskedService
}
