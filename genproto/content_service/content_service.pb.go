// Code generated by protoc-gen-go. DO NOT EDIT.
// source: content_service.proto

package content_service

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("content_service.proto", fileDescriptor_71e43a8979275517) }

var fileDescriptor_71e43a8979275517 = []byte{
	// 82 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x4d, 0xce, 0xcf, 0x2b,
	0x49, 0xcd, 0x2b, 0x89, 0x2f, 0x4e, 0x2d, 0x2a, 0xcb, 0x4c, 0x4e, 0xd5, 0x2b, 0x28, 0xca, 0x2f,
	0xc9, 0x17, 0xe2, 0x47, 0x13, 0x96, 0x92, 0x48, 0x2b, 0x4a, 0x2d, 0x8c, 0x4f, 0x2c, 0xce, 0x4e,
	0x4d, 0x41, 0x55, 0xea, 0x24, 0x15, 0x25, 0x91, 0x9e, 0x9a, 0x07, 0x66, 0xeb, 0xa3, 0xe9, 0x4a,
	0x62, 0x03, 0x0b, 0x1b, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff, 0xcc, 0x13, 0x5b, 0x92, 0x66, 0x00,
	0x00, 0x00,
}
